# Simple docs app
Clone this repo
```sh
$ git clone https://bitbucket.org/ikleeen/doc_app
```
For deploy the application
```sh
$ cd doc_app/
$ mvn -e wildfly:deploy
```
Open WEB browser and go to http://localhost:8080/docs_app-0.1-SNAPSHOT. 

For authorisation you can use this KV pair: 
* "kleen:qwerty"
* "kate:123456"

For undeploy the application
```sh
$ mvn wildfly:undeploy
```
