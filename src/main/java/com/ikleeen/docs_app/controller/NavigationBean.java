package com.ikleeen.docs_app.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "navigationBean")
@SessionScoped
public class NavigationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public String toLogin() {
		return "/login.xhtml";
	}
	
	public String toInfo() {
		return "/info.xhtml";
	}
	
	public String toWelcome() {
		return "/secured/welcome.xhtml";
	}
	
}
