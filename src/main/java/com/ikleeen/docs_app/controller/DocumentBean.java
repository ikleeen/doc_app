package com.ikleeen.docs_app.controller;

import java.io.Serializable;
import java.util.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "documentBean")
@SessionScoped
public class DocumentBean implements Serializable{
 
    private static final long serialVersionUID = 1L;
 
    private String name;
    private String comments;
    private List<Field> fields;

    public DocumentBean()
    {
        fields = new ArrayList();

        fields.add(new Field());
    }

    public void setFields(List<Field> newFields)
    {
       this.fields = newFields;
    }

    public List<Field> getFields()
    {
        return this.fields;
    }

    public void onButtonRemoveFieldClick(final Field p_oField)
    {
        fields.remove(p_oField);
    }

    public void onButtonAddFieldClick()
    {
        fields.add(new Field());
    }

   
    public String getName()
    {
        return name;
    }
 
    public void setName(String name)
    {
        this.name = name;
    }
 
        public String getComments()
    {
        return comments;
    }
 
    public void setComments(String comments)
    {
        this.comments = comments;
    }
    

    public String getSendDocument()
    {
        if("".equals(name) || name == null)
        {   return "" ; }
        else{
            return "Message : Welcome " + name + comments;
            }
    }
 
}