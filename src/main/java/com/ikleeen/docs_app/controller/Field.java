package com.ikleeen.docs_app.controller;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;

@ManagedBean(name = "field")
@SessionScoped

public class Field implements Serializable
{
    private Part file;

    public void setValue(Part p_sName)
    {
        file = p_sName;
    }

    public Part getValue()
    {
        return file;
    }
}
